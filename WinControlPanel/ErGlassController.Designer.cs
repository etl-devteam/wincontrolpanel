﻿namespace WinControlPanel
{
    partial class ErGlassController
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErGlassController));
            this.TimerImu = new System.Windows.Forms.Timer(this.components);
            this.WebCameraControl = new WebEye.Controls.WinForms.WebCameraControl.WebCameraControl();
            this.KryptonPanelTop = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.LabelKryptonExplorer = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.PictureBoxRight = new System.Windows.Forms.PictureBox();
            this.kryptonPageDocking = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.KryptonNavigator = new ComponentFactory.Krypton.Navigator.KryptonNavigator();
            this.KryptonPageData = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.KryptonGroupBoxVibration = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.KryptonButtonVib = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonNumericUpDownVibration = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.KryptonLabelMs = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonGroupBoxSensors = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.KryptonLabelGyroZ = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelGyroY = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelAccZ = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelAccY = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelGyroX = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelAccX = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelGyroscope = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelAcc = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonCheckBoxSensors = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.KryptonGroupBoxInfo = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.KryptonLabelBootFill = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelProtFill = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelCdcFill = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelErGlsFill = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelVerFill = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelDateFill = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonProtocolLabel = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonBootLabel = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonErGlassLabel = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonFirmwareLabel = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonCdcLabel = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonDateLabel = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonPageHoloDisplay = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.KryptonButtonClear = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonButtonSend = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonTextBox = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.KryptonTrackBarContrast = new ComponentFactory.Krypton.Toolkit.KryptonTrackBar();
            this.KryptonTrackBarBrightness = new ComponentFactory.Krypton.Toolkit.KryptonTrackBar();
            this.KryptonLabelContrast = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonLabelBrightness = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonButtonBrowse = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonButtonScreenshot = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonButtonDisplay = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.CentralPictureBox = new System.Windows.Forms.PictureBox();
            this.KryptonPageCamera = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.KryptonButtonReadQR = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonComboBoxCamera = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.KryptonButtonCapture = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonButtonStartCapturing = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonButtonStopCapturing = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonGroupBoxLed = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.KryptonRadioButtonRed = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.KryptonRadioButtonOff = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.KryptonRadioButtonGreen = new ComponentFactory.Krypton.Toolkit.KryptonRadioButton();
            this.KryptonPageTouchpad = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.KryptonGroupBoxTouchpad = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.KryptonLabelTouchpad = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonCheckBoxEnableTouchpad = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPanelTop)).BeginInit();
            this.KryptonPanelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPageDocking)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonNavigator)).BeginInit();
            this.KryptonNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageData)).BeginInit();
            this.KryptonPageData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxVibration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxVibration.Panel)).BeginInit();
            this.KryptonGroupBoxVibration.Panel.SuspendLayout();
            this.KryptonGroupBoxVibration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxSensors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxSensors.Panel)).BeginInit();
            this.KryptonGroupBoxSensors.Panel.SuspendLayout();
            this.KryptonGroupBoxSensors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxInfo.Panel)).BeginInit();
            this.KryptonGroupBoxInfo.Panel.SuspendLayout();
            this.KryptonGroupBoxInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageHoloDisplay)).BeginInit();
            this.KryptonPageHoloDisplay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CentralPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageCamera)).BeginInit();
            this.KryptonPageCamera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonComboBoxCamera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxLed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxLed.Panel)).BeginInit();
            this.KryptonGroupBoxLed.Panel.SuspendLayout();
            this.KryptonGroupBoxLed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageTouchpad)).BeginInit();
            this.KryptonPageTouchpad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxTouchpad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxTouchpad.Panel)).BeginInit();
            this.KryptonGroupBoxTouchpad.Panel.SuspendLayout();
            this.KryptonGroupBoxTouchpad.SuspendLayout();
            this.SuspendLayout();
            // 
            // TimerImu
            // 
            this.TimerImu.Interval = 200;
            this.TimerImu.Tick += new System.EventHandler(this.TimerImu_Tick);
            // 
            // WebCameraControl
            // 
            this.WebCameraControl.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.WebCameraControl.Location = new System.Drawing.Point(85, 94);
            this.WebCameraControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.WebCameraControl.Name = "WebCameraControl";
            this.WebCameraControl.Size = new System.Drawing.Size(473, 246);
            this.WebCameraControl.TabIndex = 45;
            // 
            // KryptonPanelTop
            // 
            this.KryptonPanelTop.Controls.Add(this.LabelKryptonExplorer);
            this.KryptonPanelTop.Controls.Add(this.PictureBoxRight);
            this.KryptonPanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.KryptonPanelTop.Location = new System.Drawing.Point(0, 0);
            this.KryptonPanelTop.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonPanelTop.Name = "KryptonPanelTop";
            this.KryptonPanelTop.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonPanelTop.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ControlClient;
            this.KryptonPanelTop.Size = new System.Drawing.Size(895, 111);
            this.KryptonPanelTop.TabIndex = 83;
            // 
            // LabelKryptonExplorer
            // 
            this.LabelKryptonExplorer.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.TitleControl;
            this.LabelKryptonExplorer.Location = new System.Drawing.Point(23, 66);
            this.LabelKryptonExplorer.Margin = new System.Windows.Forms.Padding(4);
            this.LabelKryptonExplorer.Name = "LabelKryptonExplorer";
            this.LabelKryptonExplorer.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.LabelKryptonExplorer.Size = new System.Drawing.Size(251, 34);
            this.LabelKryptonExplorer.StateCommon.ShortText.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelKryptonExplorer.StateCommon.ShortText.Hint = ComponentFactory.Krypton.Toolkit.PaletteTextHint.AntiAlias;
            this.LabelKryptonExplorer.TabIndex = 0;
            this.LabelKryptonExplorer.Values.Text = "Control Panel 3.2";
            // 
            // PictureBoxRight
            // 
            this.PictureBoxRight.BackColor = System.Drawing.SystemColors.Window;
            this.PictureBoxRight.BackgroundImage = global::WinControlPanel.Properties.Resources.Logo_Univet_Istituzionale_RGB_black;
            this.PictureBoxRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBoxRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PictureBoxRight.InitialImage = null;
            this.PictureBoxRight.Location = new System.Drawing.Point(204, 0);
            this.PictureBoxRight.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBoxRight.Name = "PictureBoxRight";
            this.PictureBoxRight.Size = new System.Drawing.Size(691, 111);
            this.PictureBoxRight.TabIndex = 0;
            this.PictureBoxRight.TabStop = false;
            // 
            // kryptonPageDocking
            // 
            this.kryptonPageDocking.AutoHiddenSlideSize = new System.Drawing.Size(150, 150);
            this.kryptonPageDocking.Flags = 62;
            this.kryptonPageDocking.LastVisibleSet = true;
            this.kryptonPageDocking.MinimumSize = new System.Drawing.Size(50, 50);
            this.kryptonPageDocking.Name = "kryptonPageDocking";
            this.kryptonPageDocking.Size = new System.Drawing.Size(585, 426);
            this.kryptonPageDocking.Text = "Docking";
            this.kryptonPageDocking.TextDescription = "";
            this.kryptonPageDocking.TextTitle = "";
            this.kryptonPageDocking.ToolTipTitle = "Page ToolTip";
            this.kryptonPageDocking.UniqueName = "4A92F3307DFB48EC4A92F3307DFB48EC";
            // 
            // KryptonNavigator
            // 
            this.KryptonNavigator.Bar.BarMapExtraText = ComponentFactory.Krypton.Navigator.MapKryptonPageText.Title;
            this.KryptonNavigator.Bar.BarMapText = ComponentFactory.Krypton.Navigator.MapKryptonPageText.Text;
            this.KryptonNavigator.Bar.BarOrientation = ComponentFactory.Krypton.Toolkit.VisualOrientation.Left;
            this.KryptonNavigator.Bar.CheckButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.LowProfile;
            this.KryptonNavigator.Bar.ItemOrientation = ComponentFactory.Krypton.Toolkit.ButtonOrientation.FixedTop;
            this.KryptonNavigator.Bar.TabBorderStyle = ComponentFactory.Krypton.Toolkit.TabBorderStyle.OneNote;
            this.KryptonNavigator.Button.ButtonDisplayLogic = ComponentFactory.Krypton.Navigator.ButtonDisplayLogic.None;
            this.KryptonNavigator.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide;
            this.KryptonNavigator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KryptonNavigator.Location = new System.Drawing.Point(0, 111);
            this.KryptonNavigator.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonNavigator.Name = "KryptonNavigator";
            this.KryptonNavigator.NavigatorMode = ComponentFactory.Krypton.Navigator.NavigatorMode.BarCheckButtonGroupOutside;
            this.KryptonNavigator.PageBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelClient;
            this.KryptonNavigator.Pages.AddRange(new ComponentFactory.Krypton.Navigator.KryptonPage[] {
            this.KryptonPageData,
            this.KryptonPageHoloDisplay,
            this.KryptonPageCamera,
            this.KryptonPageTouchpad});
            this.KryptonNavigator.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            this.KryptonNavigator.Panel.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelAlternate;
            this.KryptonNavigator.SelectedIndex = 0;
            this.KryptonNavigator.Size = new System.Drawing.Size(895, 527);
            this.KryptonNavigator.StateCommon.Bar.BarPaddingOutside = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.KryptonNavigator.StateCommon.Bar.CheckButtonGap = 5;
            this.KryptonNavigator.StateCommon.CheckButton.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.KryptonNavigator.StateCommon.CheckButton.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.KryptonNavigator.StateCommon.CheckButton.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KryptonNavigator.TabIndex = 84;
            this.KryptonNavigator.Text = "KryptonNavigatorMain";
            // 
            // KryptonPageData
            // 
            this.KryptonPageData.AutoHiddenSlideSize = new System.Drawing.Size(150, 150);
            this.KryptonPageData.Controls.Add(this.KryptonGroupBoxVibration);
            this.KryptonPageData.Controls.Add(this.KryptonGroupBoxSensors);
            this.KryptonPageData.Controls.Add(this.KryptonGroupBoxInfo);
            this.KryptonPageData.Flags = 62;
            this.KryptonPageData.ImageLarge = ((System.Drawing.Image)(resources.GetObject("KryptonPageData.ImageLarge")));
            this.KryptonPageData.ImageMedium = ((System.Drawing.Image)(resources.GetObject("KryptonPageData.ImageMedium")));
            this.KryptonPageData.LastVisibleSet = true;
            this.KryptonPageData.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonPageData.MinimumSize = new System.Drawing.Size(67, 62);
            this.KryptonPageData.Name = "KryptonPageData";
            this.KryptonPageData.Size = new System.Drawing.Size(683, 525);
            this.KryptonPageData.Text = "Data";
            this.KryptonPageData.TextTitle = "";
            this.KryptonPageData.ToolTipTitle = "Page ToolTip";
            this.KryptonPageData.UniqueName = "F1890C23D1634D34F1890C23D1634D34";
            // 
            // KryptonGroupBoxVibration
            // 
            this.KryptonGroupBoxVibration.Location = new System.Drawing.Point(169, 293);
            this.KryptonGroupBoxVibration.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonGroupBoxVibration.Name = "KryptonGroupBoxVibration";
            this.KryptonGroupBoxVibration.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            // 
            // KryptonGroupBoxVibration.Panel
            // 
            this.KryptonGroupBoxVibration.Panel.Controls.Add(this.KryptonButtonVib);
            this.KryptonGroupBoxVibration.Panel.Controls.Add(this.KryptonNumericUpDownVibration);
            this.KryptonGroupBoxVibration.Panel.Controls.Add(this.KryptonLabelMs);
            this.KryptonGroupBoxVibration.Size = new System.Drawing.Size(325, 194);
            this.KryptonGroupBoxVibration.TabIndex = 8;
            this.KryptonGroupBoxVibration.Values.Heading = "Vibration";
            // 
            // KryptonButtonVib
            // 
            this.KryptonButtonVib.Location = new System.Drawing.Point(164, 60);
            this.KryptonButtonVib.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonVib.Name = "KryptonButtonVib";
            this.KryptonButtonVib.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonVib.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonVib.TabIndex = 1;
            this.KryptonButtonVib.Values.Text = "Vibrate";
            this.KryptonButtonVib.Click += new System.EventHandler(this.VibrationBtn_Click);
            // 
            // KryptonNumericUpDownVibration
            // 
            this.KryptonNumericUpDownVibration.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.KryptonNumericUpDownVibration.Location = new System.Drawing.Point(32, 62);
            this.KryptonNumericUpDownVibration.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonNumericUpDownVibration.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.KryptonNumericUpDownVibration.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.KryptonNumericUpDownVibration.Name = "KryptonNumericUpDownVibration";
            this.KryptonNumericUpDownVibration.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonNumericUpDownVibration.Size = new System.Drawing.Size(87, 26);
            this.KryptonNumericUpDownVibration.StateCommon.Content.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.KryptonNumericUpDownVibration.TabIndex = 0;
            this.KryptonNumericUpDownVibration.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.KryptonNumericUpDownVibration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.VibrUpDown_KeyPress);
            // 
            // KryptonLabelMs
            // 
            this.KryptonLabelMs.Location = new System.Drawing.Point(113, 62);
            this.KryptonLabelMs.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelMs.Name = "KryptonLabelMs";
            this.KryptonLabelMs.Size = new System.Drawing.Size(31, 24);
            this.KryptonLabelMs.TabIndex = 9;
            this.KryptonLabelMs.Values.Text = "ms";
            // 
            // KryptonGroupBoxSensors
            // 
            this.KryptonGroupBoxSensors.Location = new System.Drawing.Point(336, 6);
            this.KryptonGroupBoxSensors.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonGroupBoxSensors.Name = "KryptonGroupBoxSensors";
            this.KryptonGroupBoxSensors.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            // 
            // KryptonGroupBoxSensors.Panel
            // 
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonLabelGyroZ);
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonLabelGyroY);
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonLabelAccZ);
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonLabelAccY);
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonLabelGyroX);
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonLabelAccX);
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonLabelGyroscope);
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonLabelAcc);
            this.KryptonGroupBoxSensors.Panel.Controls.Add(this.KryptonCheckBoxSensors);
            this.KryptonGroupBoxSensors.Size = new System.Drawing.Size(329, 251);
            this.KryptonGroupBoxSensors.TabIndex = 7;
            this.KryptonGroupBoxSensors.Values.Heading = "Sensors";
            // 
            // KryptonLabelGyroZ
            // 
            this.KryptonLabelGyroZ.Location = new System.Drawing.Point(153, 146);
            this.KryptonLabelGyroZ.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelGyroZ.Name = "KryptonLabelGyroZ";
            this.KryptonLabelGyroZ.Size = new System.Drawing.Size(35, 24);
            this.KryptonLabelGyroZ.TabIndex = 8;
            this.KryptonLabelGyroZ.Values.Text = "Z = ";
            // 
            // KryptonLabelGyroY
            // 
            this.KryptonLabelGyroY.Location = new System.Drawing.Point(153, 114);
            this.KryptonLabelGyroY.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelGyroY.Name = "KryptonLabelGyroY";
            this.KryptonLabelGyroY.Size = new System.Drawing.Size(35, 24);
            this.KryptonLabelGyroY.TabIndex = 7;
            this.KryptonLabelGyroY.Values.Text = "Y = ";
            // 
            // KryptonLabelAccZ
            // 
            this.KryptonLabelAccZ.Location = new System.Drawing.Point(17, 146);
            this.KryptonLabelAccZ.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelAccZ.Name = "KryptonLabelAccZ";
            this.KryptonLabelAccZ.Size = new System.Drawing.Size(35, 24);
            this.KryptonLabelAccZ.TabIndex = 6;
            this.KryptonLabelAccZ.Values.Text = "Z = ";
            // 
            // KryptonLabelAccY
            // 
            this.KryptonLabelAccY.Location = new System.Drawing.Point(17, 114);
            this.KryptonLabelAccY.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelAccY.Name = "KryptonLabelAccY";
            this.KryptonLabelAccY.Size = new System.Drawing.Size(35, 24);
            this.KryptonLabelAccY.TabIndex = 5;
            this.KryptonLabelAccY.Values.Text = "Y = ";
            // 
            // KryptonLabelGyroX
            // 
            this.KryptonLabelGyroX.Location = new System.Drawing.Point(153, 84);
            this.KryptonLabelGyroX.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelGyroX.Name = "KryptonLabelGyroX";
            this.KryptonLabelGyroX.Size = new System.Drawing.Size(35, 24);
            this.KryptonLabelGyroX.TabIndex = 4;
            this.KryptonLabelGyroX.Values.Text = "X = ";
            // 
            // KryptonLabelAccX
            // 
            this.KryptonLabelAccX.Location = new System.Drawing.Point(17, 84);
            this.KryptonLabelAccX.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelAccX.Name = "KryptonLabelAccX";
            this.KryptonLabelAccX.Size = new System.Drawing.Size(35, 24);
            this.KryptonLabelAccX.TabIndex = 3;
            this.KryptonLabelAccX.Values.Text = "X = ";
            // 
            // KryptonLabelGyroscope
            // 
            this.KryptonLabelGyroscope.Location = new System.Drawing.Point(157, 52);
            this.KryptonLabelGyroscope.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelGyroscope.Name = "KryptonLabelGyroscope";
            this.KryptonLabelGyroscope.Size = new System.Drawing.Size(84, 24);
            this.KryptonLabelGyroscope.TabIndex = 2;
            this.KryptonLabelGyroscope.Values.Text = "Gyroscope";
            // 
            // KryptonLabelAcc
            // 
            this.KryptonLabelAcc.Location = new System.Drawing.Point(17, 52);
            this.KryptonLabelAcc.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelAcc.Name = "KryptonLabelAcc";
            this.KryptonLabelAcc.Size = new System.Drawing.Size(110, 24);
            this.KryptonLabelAcc.TabIndex = 1;
            this.KryptonLabelAcc.Values.Text = "Accelerometer";
            // 
            // KryptonCheckBoxSensors
            // 
            this.KryptonCheckBoxSensors.Location = new System.Drawing.Point(117, 4);
            this.KryptonCheckBoxSensors.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonCheckBoxSensors.Name = "KryptonCheckBoxSensors";
            this.KryptonCheckBoxSensors.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonCheckBoxSensors.Size = new System.Drawing.Size(88, 24);
            this.KryptonCheckBoxSensors.TabIndex = 0;
            this.KryptonCheckBoxSensors.Values.Text = "read IMU";
            this.KryptonCheckBoxSensors.CheckedChanged += new System.EventHandler(this.ReadSensorsCheckBox_CheckedChanged);
            // 
            // KryptonGroupBoxInfo
            // 
            this.KryptonGroupBoxInfo.Location = new System.Drawing.Point(4, 6);
            this.KryptonGroupBoxInfo.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonGroupBoxInfo.Name = "KryptonGroupBoxInfo";
            this.KryptonGroupBoxInfo.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            // 
            // KryptonGroupBoxInfo.Panel
            // 
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonLabelBootFill);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonLabelProtFill);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonLabelCdcFill);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonLabelErGlsFill);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonLabelVerFill);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonLabelDateFill);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonProtocolLabel);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonBootLabel);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonErGlassLabel);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonFirmwareLabel);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonCdcLabel);
            this.KryptonGroupBoxInfo.Panel.Controls.Add(this.KryptonDateLabel);
            this.KryptonGroupBoxInfo.Size = new System.Drawing.Size(324, 251);
            this.KryptonGroupBoxInfo.TabIndex = 6;
            this.KryptonGroupBoxInfo.Values.Heading = "Info";
            // 
            // KryptonLabelBootFill
            // 
            this.KryptonLabelBootFill.Location = new System.Drawing.Point(165, 52);
            this.KryptonLabelBootFill.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelBootFill.Name = "KryptonLabelBootFill";
            this.KryptonLabelBootFill.Size = new System.Drawing.Size(6, 2);
            this.KryptonLabelBootFill.TabIndex = 11;
            this.KryptonLabelBootFill.Values.Text = "";
            // 
            // KryptonLabelProtFill
            // 
            this.KryptonLabelProtFill.Location = new System.Drawing.Point(165, 20);
            this.KryptonLabelProtFill.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelProtFill.Name = "KryptonLabelProtFill";
            this.KryptonLabelProtFill.Size = new System.Drawing.Size(6, 2);
            this.KryptonLabelProtFill.TabIndex = 10;
            this.KryptonLabelProtFill.Values.Text = "";
            // 
            // KryptonLabelCdcFill
            // 
            this.KryptonLabelCdcFill.Location = new System.Drawing.Point(165, 148);
            this.KryptonLabelCdcFill.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelCdcFill.Name = "KryptonLabelCdcFill";
            this.KryptonLabelCdcFill.Size = new System.Drawing.Size(6, 2);
            this.KryptonLabelCdcFill.TabIndex = 9;
            this.KryptonLabelCdcFill.Values.Text = "";
            // 
            // KryptonLabelErGlsFill
            // 
            this.KryptonLabelErGlsFill.Location = new System.Drawing.Point(165, 180);
            this.KryptonLabelErGlsFill.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelErGlsFill.Name = "KryptonLabelErGlsFill";
            this.KryptonLabelErGlsFill.Size = new System.Drawing.Size(6, 2);
            this.KryptonLabelErGlsFill.TabIndex = 8;
            this.KryptonLabelErGlsFill.Values.Text = "";
            // 
            // KryptonLabelVerFill
            // 
            this.KryptonLabelVerFill.Location = new System.Drawing.Point(165, 84);
            this.KryptonLabelVerFill.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelVerFill.Name = "KryptonLabelVerFill";
            this.KryptonLabelVerFill.Size = new System.Drawing.Size(6, 2);
            this.KryptonLabelVerFill.TabIndex = 7;
            this.KryptonLabelVerFill.Values.Text = "";
            // 
            // KryptonLabelDateFill
            // 
            this.KryptonLabelDateFill.Location = new System.Drawing.Point(165, 116);
            this.KryptonLabelDateFill.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelDateFill.Name = "KryptonLabelDateFill";
            this.KryptonLabelDateFill.Size = new System.Drawing.Size(6, 2);
            this.KryptonLabelDateFill.TabIndex = 6;
            this.KryptonLabelDateFill.Values.Text = "";
            // 
            // KryptonProtocolLabel
            // 
            this.KryptonProtocolLabel.Location = new System.Drawing.Point(4, 20);
            this.KryptonProtocolLabel.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonProtocolLabel.Name = "KryptonProtocolLabel";
            this.KryptonProtocolLabel.Size = new System.Drawing.Size(132, 24);
            this.KryptonProtocolLabel.TabIndex = 0;
            this.KryptonProtocolLabel.Values.Text = " Protocol Version:";
            // 
            // KryptonBootLabel
            // 
            this.KryptonBootLabel.Location = new System.Drawing.Point(4, 52);
            this.KryptonBootLabel.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonBootLabel.Name = "KryptonBootLabel";
            this.KryptonBootLabel.Size = new System.Drawing.Size(107, 24);
            this.KryptonBootLabel.TabIndex = 5;
            this.KryptonBootLabel.Values.Text = " Boot Version:";
            // 
            // KryptonErGlassLabel
            // 
            this.KryptonErGlassLabel.Location = new System.Drawing.Point(4, 180);
            this.KryptonErGlassLabel.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonErGlassLabel.Name = "KryptonErGlassLabel";
            this.KryptonErGlassLabel.Size = new System.Drawing.Size(114, 24);
            this.KryptonErGlassLabel.TabIndex = 1;
            this.KryptonErGlassLabel.Values.Text = " Status ErGlass:";
            // 
            // KryptonFirmwareLabel
            // 
            this.KryptonFirmwareLabel.Location = new System.Drawing.Point(4, 84);
            this.KryptonFirmwareLabel.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonFirmwareLabel.Name = "KryptonFirmwareLabel";
            this.KryptonFirmwareLabel.Size = new System.Drawing.Size(137, 24);
            this.KryptonFirmwareLabel.TabIndex = 4;
            this.KryptonFirmwareLabel.Values.Text = " Firmware Version:";
            // 
            // KryptonCdcLabel
            // 
            this.KryptonCdcLabel.Location = new System.Drawing.Point(4, 148);
            this.KryptonCdcLabel.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonCdcLabel.Name = "KryptonCdcLabel";
            this.KryptonCdcLabel.Size = new System.Drawing.Size(91, 24);
            this.KryptonCdcLabel.TabIndex = 2;
            this.KryptonCdcLabel.Values.Text = " Status Cdc:";
            // 
            // KryptonDateLabel
            // 
            this.KryptonDateLabel.Location = new System.Drawing.Point(4, 116);
            this.KryptonDateLabel.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonDateLabel.Name = "KryptonDateLabel";
            this.KryptonDateLabel.Size = new System.Drawing.Size(156, 24);
            this.KryptonDateLabel.TabIndex = 3;
            this.KryptonDateLabel.Values.Text = " Firmware Build Date:";
            // 
            // KryptonPageHoloDisplay
            // 
            this.KryptonPageHoloDisplay.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonButtonClear);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonButtonSend);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonTextBox);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonTrackBarContrast);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonTrackBarBrightness);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonLabelContrast);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonLabelBrightness);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonButtonBrowse);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonButtonScreenshot);
            this.KryptonPageHoloDisplay.Controls.Add(this.KryptonButtonDisplay);
            this.KryptonPageHoloDisplay.Controls.Add(this.CentralPictureBox);
            this.KryptonPageHoloDisplay.Flags = 65534;
            this.KryptonPageHoloDisplay.LastVisibleSet = true;
            this.KryptonPageHoloDisplay.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonPageHoloDisplay.MinimumSize = new System.Drawing.Size(67, 62);
            this.KryptonPageHoloDisplay.Name = "KryptonPageHoloDisplay";
            this.KryptonPageHoloDisplay.Size = new System.Drawing.Size(660, 524);
            this.KryptonPageHoloDisplay.Text = "Holographic Display";
            this.KryptonPageHoloDisplay.TextDescription = "";
            this.KryptonPageHoloDisplay.TextTitle = "";
            this.KryptonPageHoloDisplay.ToolTipTitle = "Page ToolTip";
            this.KryptonPageHoloDisplay.UniqueName = "B708E9ADD440465331B68F76E239DB31";
            // 
            // KryptonButtonClear
            // 
            this.KryptonButtonClear.Location = new System.Drawing.Point(412, 324);
            this.KryptonButtonClear.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonClear.Name = "KryptonButtonClear";
            this.KryptonButtonClear.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonClear.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonClear.TabIndex = 10;
            this.KryptonButtonClear.Values.Text = "Clear";
            this.KryptonButtonClear.Click += new System.EventHandler(this.KryptonButtonClear_Click);
            // 
            // KryptonButtonSend
            // 
            this.KryptonButtonSend.Location = new System.Drawing.Point(412, 265);
            this.KryptonButtonSend.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonSend.Name = "KryptonButtonSend";
            this.KryptonButtonSend.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonSend.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonSend.TabIndex = 9;
            this.KryptonButtonSend.Values.Text = "Send";
            this.KryptonButtonSend.Click += new System.EventHandler(this.KryptonButtonSend_Click);
            // 
            // KryptonTextBox
            // 
            this.KryptonTextBox.AlwaysActive = false;
            this.KryptonTextBox.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Ribbon;
            this.KryptonTextBox.Location = new System.Drawing.Point(52, 256);
            this.KryptonTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonTextBox.Multiline = true;
            this.KryptonTextBox.Name = "KryptonTextBox";
            this.KryptonTextBox.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            this.KryptonTextBox.Size = new System.Drawing.Size(307, 117);
            this.KryptonTextBox.TabIndex = 8;
            this.KryptonTextBox.Text = "Input text here";
            this.KryptonTextBox.TextChanged += new System.EventHandler(this.KryptonTextBox_TextChanged);
            // 
            // KryptonTrackBarContrast
            // 
            this.KryptonTrackBarContrast.DrawBackground = true;
            this.KryptonTrackBarContrast.Location = new System.Drawing.Point(357, 436);
            this.KryptonTrackBarContrast.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonTrackBarContrast.Maximum = 255;
            this.KryptonTrackBarContrast.Minimum = 1;
            this.KryptonTrackBarContrast.Name = "KryptonTrackBarContrast";
            this.KryptonTrackBarContrast.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonTrackBarContrast.Size = new System.Drawing.Size(223, 33);
            this.KryptonTrackBarContrast.TabIndex = 7;
            this.KryptonTrackBarContrast.TickFrequency = 10;
            this.KryptonTrackBarContrast.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.KryptonTrackBarContrast.Value = 127;
            this.KryptonTrackBarContrast.Scroll += new System.EventHandler(this.ContrastTrackbar_Scroll);
            // 
            // KryptonTrackBarBrightness
            // 
            this.KryptonTrackBarBrightness.DrawBackground = true;
            this.KryptonTrackBarBrightness.Location = new System.Drawing.Point(23, 436);
            this.KryptonTrackBarBrightness.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonTrackBarBrightness.Maximum = 255;
            this.KryptonTrackBarBrightness.Minimum = 1;
            this.KryptonTrackBarBrightness.Name = "KryptonTrackBarBrightness";
            this.KryptonTrackBarBrightness.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonTrackBarBrightness.Size = new System.Drawing.Size(223, 33);
            this.KryptonTrackBarBrightness.TabIndex = 6;
            this.KryptonTrackBarBrightness.TickFrequency = 10;
            this.KryptonTrackBarBrightness.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.KryptonTrackBarBrightness.Value = 127;
            this.KryptonTrackBarBrightness.Scroll += new System.EventHandler(this.BrightnessTrackbar_Scroll);
            // 
            // KryptonLabelContrast
            // 
            this.KryptonLabelContrast.Location = new System.Drawing.Point(431, 396);
            this.KryptonLabelContrast.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelContrast.Name = "KryptonLabelContrast";
            this.KryptonLabelContrast.Size = new System.Drawing.Size(69, 24);
            this.KryptonLabelContrast.TabIndex = 5;
            this.KryptonLabelContrast.Values.Text = "Contrast";
            // 
            // KryptonLabelBrightness
            // 
            this.KryptonLabelBrightness.Location = new System.Drawing.Point(89, 396);
            this.KryptonLabelBrightness.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelBrightness.Name = "KryptonLabelBrightness";
            this.KryptonLabelBrightness.Size = new System.Drawing.Size(82, 24);
            this.KryptonLabelBrightness.TabIndex = 4;
            this.KryptonLabelBrightness.Values.Text = "Brightness";
            // 
            // KryptonButtonBrowse
            // 
            this.KryptonButtonBrowse.Location = new System.Drawing.Point(432, 192);
            this.KryptonButtonBrowse.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonBrowse.Name = "KryptonButtonBrowse";
            this.KryptonButtonBrowse.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonBrowse.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonBrowse.TabIndex = 3;
            this.KryptonButtonBrowse.Values.Text = "Browse";
            this.KryptonButtonBrowse.Click += new System.EventHandler(this.BrowseBtn_Click);
            // 
            // KryptonButtonScreenshot
            // 
            this.KryptonButtonScreenshot.Location = new System.Drawing.Point(272, 192);
            this.KryptonButtonScreenshot.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonScreenshot.Name = "KryptonButtonScreenshot";
            this.KryptonButtonScreenshot.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonScreenshot.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonScreenshot.TabIndex = 2;
            this.KryptonButtonScreenshot.Values.Text = "Screenshot";
            this.KryptonButtonScreenshot.Click += new System.EventHandler(this.ScreenshotBtn_Click);
            // 
            // KryptonButtonDisplay
            // 
            this.KryptonButtonDisplay.Location = new System.Drawing.Point(112, 192);
            this.KryptonButtonDisplay.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonDisplay.Name = "KryptonButtonDisplay";
            this.KryptonButtonDisplay.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonDisplay.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonDisplay.TabIndex = 1;
            this.KryptonButtonDisplay.Values.Text = "Display";
            this.KryptonButtonDisplay.Click += new System.EventHandler(this.DisplayBtn_Click);
            // 
            // CentralPictureBox
            // 
            this.CentralPictureBox.Location = new System.Drawing.Point(52, 12);
            this.CentralPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.CentralPictureBox.Name = "CentralPictureBox";
            this.CentralPictureBox.Size = new System.Drawing.Size(559, 170);
            this.CentralPictureBox.TabIndex = 0;
            this.CentralPictureBox.TabStop = false;
            // 
            // KryptonPageCamera
            // 
            this.KryptonPageCamera.AutoHiddenSlideSize = new System.Drawing.Size(150, 150);
            this.KryptonPageCamera.Controls.Add(this.KryptonButtonReadQR);
            this.KryptonPageCamera.Controls.Add(this.KryptonComboBoxCamera);
            this.KryptonPageCamera.Controls.Add(this.KryptonButtonCapture);
            this.KryptonPageCamera.Controls.Add(this.KryptonButtonStartCapturing);
            this.KryptonPageCamera.Controls.Add(this.KryptonButtonStopCapturing);
            this.KryptonPageCamera.Controls.Add(this.KryptonGroupBoxLed);
            this.KryptonPageCamera.Controls.Add(this.WebCameraControl);
            this.KryptonPageCamera.Flags = 62;
            this.KryptonPageCamera.LastVisibleSet = true;
            this.KryptonPageCamera.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonPageCamera.MinimumSize = new System.Drawing.Size(67, 62);
            this.KryptonPageCamera.Name = "KryptonPageCamera";
            this.KryptonPageCamera.Size = new System.Drawing.Size(660, 524);
            this.KryptonPageCamera.Text = "Camera";
            this.KryptonPageCamera.TextDescription = "";
            this.KryptonPageCamera.TextTitle = "";
            this.KryptonPageCamera.ToolTipTitle = "Page ToolTip";
            this.KryptonPageCamera.UniqueName = "4A92F3307DFB48EC4A92F3307DFB48EC";
            // 
            // KryptonButtonReadQR
            // 
            this.KryptonButtonReadQR.Enabled = false;
            this.KryptonButtonReadQR.Location = new System.Drawing.Point(463, 367);
            this.KryptonButtonReadQR.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonReadQR.Name = "KryptonButtonReadQR";
            this.KryptonButtonReadQR.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonReadQR.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonReadQR.TabIndex = 50;
            this.KryptonButtonReadQR.Values.Text = "Read QR";
            this.KryptonButtonReadQR.Click += new System.EventHandler(this.ReadQRBtn_Click);
            // 
            // KryptonComboBoxCamera
            // 
            this.KryptonComboBoxCamera.AlwaysActive = false;
            this.KryptonComboBoxCamera.DropDownWidth = 121;
            this.KryptonComboBoxCamera.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Ribbon;
            this.KryptonComboBoxCamera.IntegralHeight = false;
            this.KryptonComboBoxCamera.Items.AddRange(new object[] {
            "Please select a camera"});
            this.KryptonComboBoxCamera.Location = new System.Drawing.Point(145, 34);
            this.KryptonComboBoxCamera.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonComboBoxCamera.Name = "KryptonComboBoxCamera";
            this.KryptonComboBoxCamera.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonComboBoxCamera.Size = new System.Drawing.Size(352, 25);
            this.KryptonComboBoxCamera.StateCommon.ComboBox.Content.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.KryptonComboBoxCamera.TabIndex = 49;
            this.KryptonComboBoxCamera.Text = "Please select a camera";
            // 
            // KryptonButtonCapture
            // 
            this.KryptonButtonCapture.Enabled = false;
            this.KryptonButtonCapture.Location = new System.Drawing.Point(329, 367);
            this.KryptonButtonCapture.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonCapture.Name = "KryptonButtonCapture";
            this.KryptonButtonCapture.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonCapture.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonCapture.TabIndex = 48;
            this.KryptonButtonCapture.Values.Text = "Capture";
            this.KryptonButtonCapture.Click += new System.EventHandler(this.CaptureBtn_Click);
            // 
            // KryptonButtonStartCapturing
            // 
            this.KryptonButtonStartCapturing.Location = new System.Drawing.Point(63, 367);
            this.KryptonButtonStartCapturing.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonStartCapturing.Name = "KryptonButtonStartCapturing";
            this.KryptonButtonStartCapturing.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonStartCapturing.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonStartCapturing.TabIndex = 47;
            this.KryptonButtonStartCapturing.Values.Text = "Start";
            this.KryptonButtonStartCapturing.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // KryptonButtonStopCapturing
            // 
            this.KryptonButtonStopCapturing.Enabled = false;
            this.KryptonButtonStopCapturing.Location = new System.Drawing.Point(196, 367);
            this.KryptonButtonStopCapturing.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonButtonStopCapturing.Name = "KryptonButtonStopCapturing";
            this.KryptonButtonStopCapturing.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonButtonStopCapturing.Size = new System.Drawing.Size(120, 31);
            this.KryptonButtonStopCapturing.TabIndex = 46;
            this.KryptonButtonStopCapturing.Values.Text = "Stop";
            this.KryptonButtonStopCapturing.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // KryptonGroupBoxLed
            // 
            this.KryptonGroupBoxLed.Location = new System.Drawing.Point(167, 410);
            this.KryptonGroupBoxLed.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonGroupBoxLed.Name = "KryptonGroupBoxLed";
            this.KryptonGroupBoxLed.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            // 
            // KryptonGroupBoxLed.Panel
            // 
            this.KryptonGroupBoxLed.Panel.Controls.Add(this.KryptonRadioButtonRed);
            this.KryptonGroupBoxLed.Panel.Controls.Add(this.KryptonRadioButtonOff);
            this.KryptonGroupBoxLed.Panel.Controls.Add(this.KryptonRadioButtonGreen);
            this.KryptonGroupBoxLed.Size = new System.Drawing.Size(328, 97);
            this.KryptonGroupBoxLed.TabIndex = 9;
            this.KryptonGroupBoxLed.Values.Heading = "Led";
            // 
            // KryptonRadioButtonRed
            // 
            this.KryptonRadioButtonRed.Location = new System.Drawing.Point(5, 20);
            this.KryptonRadioButtonRed.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonRadioButtonRed.Name = "KryptonRadioButtonRed";
            this.KryptonRadioButtonRed.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonRadioButtonRed.Size = new System.Drawing.Size(50, 24);
            this.KryptonRadioButtonRed.TabIndex = 0;
            this.KryptonRadioButtonRed.Values.Text = "Red";
            this.KryptonRadioButtonRed.CheckedChanged += new System.EventHandler(this.RedRadioBtn_CheckedChanged);
            // 
            // KryptonRadioButtonOff
            // 
            this.KryptonRadioButtonOff.Checked = true;
            this.KryptonRadioButtonOff.Location = new System.Drawing.Point(233, 20);
            this.KryptonRadioButtonOff.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonRadioButtonOff.Name = "KryptonRadioButtonOff";
            this.KryptonRadioButtonOff.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonRadioButtonOff.Size = new System.Drawing.Size(45, 24);
            this.KryptonRadioButtonOff.TabIndex = 2;
            this.KryptonRadioButtonOff.Values.Text = "Off";
            this.KryptonRadioButtonOff.CheckedChanged += new System.EventHandler(this.OffRadioBtn_CheckedChanged);
            // 
            // KryptonRadioButtonGreen
            // 
            this.KryptonRadioButtonGreen.Location = new System.Drawing.Point(111, 20);
            this.KryptonRadioButtonGreen.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonRadioButtonGreen.Name = "KryptonRadioButtonGreen";
            this.KryptonRadioButtonGreen.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonRadioButtonGreen.Size = new System.Drawing.Size(64, 24);
            this.KryptonRadioButtonGreen.TabIndex = 1;
            this.KryptonRadioButtonGreen.Values.Text = "Green";
            this.KryptonRadioButtonGreen.CheckedChanged += new System.EventHandler(this.GreenRadioBtn_CheckedChanged);
            // 
            // KryptonPageTouchpad
            // 
            this.KryptonPageTouchpad.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.KryptonPageTouchpad.Controls.Add(this.KryptonGroupBoxTouchpad);
            this.KryptonPageTouchpad.Flags = 65534;
            this.KryptonPageTouchpad.LastVisibleSet = true;
            this.KryptonPageTouchpad.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonPageTouchpad.MinimumSize = new System.Drawing.Size(67, 62);
            this.KryptonPageTouchpad.Name = "KryptonPageTouchpad";
            this.KryptonPageTouchpad.Size = new System.Drawing.Size(660, 524);
            this.KryptonPageTouchpad.Text = "Touchpad";
            this.KryptonPageTouchpad.TextTitle = "";
            this.KryptonPageTouchpad.ToolTipTitle = "Page ToolTip";
            this.KryptonPageTouchpad.UniqueName = "49AFCFA363FA44EFFD9CA19B91515A24";
            // 
            // KryptonGroupBoxTouchpad
            // 
            this.KryptonGroupBoxTouchpad.Location = new System.Drawing.Point(181, 81);
            this.KryptonGroupBoxTouchpad.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonGroupBoxTouchpad.Name = "KryptonGroupBoxTouchpad";
            this.KryptonGroupBoxTouchpad.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            // 
            // KryptonGroupBoxTouchpad.Panel
            // 
            this.KryptonGroupBoxTouchpad.Panel.Controls.Add(this.KryptonLabelTouchpad);
            this.KryptonGroupBoxTouchpad.Panel.Controls.Add(this.KryptonCheckBoxEnableTouchpad);
            this.KryptonGroupBoxTouchpad.Size = new System.Drawing.Size(325, 194);
            this.KryptonGroupBoxTouchpad.TabIndex = 9;
            this.KryptonGroupBoxTouchpad.Values.Heading = "Touchpad";
            // 
            // KryptonLabelTouchpad
            // 
            this.KryptonLabelTouchpad.Location = new System.Drawing.Point(91, 86);
            this.KryptonLabelTouchpad.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonLabelTouchpad.Name = "KryptonLabelTouchpad";
            this.KryptonLabelTouchpad.Size = new System.Drawing.Size(138, 24);
            this.KryptonLabelTouchpad.TabIndex = 2;
            this.KryptonLabelTouchpad.Values.Text = "No Input Detected";
            // 
            // KryptonCheckBoxEnableTouchpad
            // 
            this.KryptonCheckBoxEnableTouchpad.Location = new System.Drawing.Point(113, 9);
            this.KryptonCheckBoxEnableTouchpad.Margin = new System.Windows.Forms.Padding(4);
            this.KryptonCheckBoxEnableTouchpad.Name = "KryptonCheckBoxEnableTouchpad";
            this.KryptonCheckBoxEnableTouchpad.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.KryptonCheckBoxEnableTouchpad.Size = new System.Drawing.Size(70, 24);
            this.KryptonCheckBoxEnableTouchpad.TabIndex = 1;
            this.KryptonCheckBoxEnableTouchpad.Values.Text = "enable";
            this.KryptonCheckBoxEnableTouchpad.CheckedChanged += new System.EventHandler(this.EnableTouchpadCheckBox_CheckedChanged);
            // 
            // ErGlassController
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 638);
            this.Controls.Add(this.KryptonNavigator);
            this.Controls.Add(this.KryptonPanelTop);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ErGlassController";
            this.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.Text = "ErGlassController";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ErGlassController_FormClosing);
            this.Load += new System.EventHandler(this.ErGlassDemo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPanelTop)).EndInit();
            this.KryptonPanelTop.ResumeLayout(false);
            this.KryptonPanelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPageDocking)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonNavigator)).EndInit();
            this.KryptonNavigator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageData)).EndInit();
            this.KryptonPageData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxVibration.Panel)).EndInit();
            this.KryptonGroupBoxVibration.Panel.ResumeLayout(false);
            this.KryptonGroupBoxVibration.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxVibration)).EndInit();
            this.KryptonGroupBoxVibration.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxSensors.Panel)).EndInit();
            this.KryptonGroupBoxSensors.Panel.ResumeLayout(false);
            this.KryptonGroupBoxSensors.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxSensors)).EndInit();
            this.KryptonGroupBoxSensors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxInfo.Panel)).EndInit();
            this.KryptonGroupBoxInfo.Panel.ResumeLayout(false);
            this.KryptonGroupBoxInfo.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxInfo)).EndInit();
            this.KryptonGroupBoxInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageHoloDisplay)).EndInit();
            this.KryptonPageHoloDisplay.ResumeLayout(false);
            this.KryptonPageHoloDisplay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CentralPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageCamera)).EndInit();
            this.KryptonPageCamera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonComboBoxCamera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxLed.Panel)).EndInit();
            this.KryptonGroupBoxLed.Panel.ResumeLayout(false);
            this.KryptonGroupBoxLed.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxLed)).EndInit();
            this.KryptonGroupBoxLed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageTouchpad)).EndInit();
            this.KryptonPageTouchpad.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxTouchpad.Panel)).EndInit();
            this.KryptonGroupBoxTouchpad.Panel.ResumeLayout(false);
            this.KryptonGroupBoxTouchpad.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxTouchpad)).EndInit();
            this.KryptonGroupBoxTouchpad.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer TimerImu;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel KryptonPanelTop;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel LabelKryptonExplorer;
        private System.Windows.Forms.PictureBox PictureBoxRight;
        private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPageDocking;
        private ComponentFactory.Krypton.Navigator.KryptonNavigator KryptonNavigator;
        private ComponentFactory.Krypton.Navigator.KryptonPage KryptonPageData;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonBootLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonFirmwareLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonDateLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonCdcLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonErGlassLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonProtocolLabel;
        private ComponentFactory.Krypton.Navigator.KryptonPage KryptonPageCamera;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox KryptonGroupBoxInfo;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox KryptonGroupBoxSensors;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelGyroZ;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelGyroY;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelAccZ;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelAccY;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelGyroX;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelAccX;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelGyroscope;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelAcc;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox KryptonCheckBoxSensors;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox KryptonGroupBoxVibration;
        internal ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown KryptonNumericUpDownVibration;
        private ComponentFactory.Krypton.Navigator.KryptonPage KryptonPageHoloDisplay;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonVib;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonBrowse;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonScreenshot;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonDisplay;
        private System.Windows.Forms.PictureBox CentralPictureBox;
        private ComponentFactory.Krypton.Toolkit.KryptonTrackBar KryptonTrackBarContrast;
        private ComponentFactory.Krypton.Toolkit.KryptonTrackBar KryptonTrackBarBrightness;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelContrast;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelBrightness;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton KryptonRadioButtonOff;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton KryptonRadioButtonGreen;
        private ComponentFactory.Krypton.Toolkit.KryptonRadioButton KryptonRadioButtonRed;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox KryptonGroupBoxLed;
        private WebEye.Controls.WinForms.WebCameraControl.WebCameraControl WebCameraControl;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonCapture;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonStartCapturing;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonStopCapturing;
        private ComponentFactory.Krypton.Navigator.KryptonPage KryptonPageTouchpad;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox KryptonGroupBoxTouchpad;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelTouchpad;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox KryptonCheckBoxEnableTouchpad;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox KryptonComboBoxCamera;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonReadQR;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelBootFill;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelProtFill;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelCdcFill;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelErGlsFill;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelVerFill;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelDateFill;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelMs;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox KryptonTextBox;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonSend;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonClear;
    }
}

