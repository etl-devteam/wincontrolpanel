﻿using ComponentFactory.Krypton.Toolkit;
using QRCodeDecoderLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using WebEye.Controls.WinForms.WebCameraControl;


namespace WinControlPanel
{
    public partial class ErGlassController : KryptonForm
    {
        private class VisionARDLL
        {
            [DllImport("VisionARFB.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern bool Startup();

            [DllImport("VisionARCDC.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern bool StartupCdc();

            [DllImport("VisionARFB.dll", EntryPoint = "CloseFb", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool CloseDevFb();

            [DllImport("VisionARCDC.dll", EntryPoint = "CloseCdc", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool CloseDevCdc();

            [DllImport("VisionARFB.dll", EntryPoint = "WriteImg", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool WriteImg(byte[] data);

            [DllImport("VisionARFB.dll", EntryPoint = "Contrast", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool Contrast(int val);

            [DllImport("VisionARFB.dll", EntryPoint = "Brightness", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool Brightness(int val);

            [DllImport("VisionARCDC.dll", EntryPoint = "Haptic", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool Haptic(int time);

            [DllImport("VisionARCDC.dll", EntryPoint = "StartTouchpad", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool StartTouchpad(Callback callback, ulong ms);

            [DllImport("VisionARCDC.dll", EntryPoint = "StopTouchpad", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool StopTouchpad();

            [DllImport("VisionARCDC.dll", EntryPoint = "GetImuAcc", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool GetImuAcc(float[] imuAcc);

            [DllImport("VisionARCDC.dll", EntryPoint = "GetImuGyro", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool GetImuGyro(float[] imuGyro);

            [DllImport("VisionARCDC.dll", EntryPoint = "GetProtVers", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool GetProtVers(byte[] protVers);

            [DllImport("VisionARCDC.dll", EntryPoint = "GetBootVers", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool GetBootVers(byte[] bootVers);

            [DllImport("VisionARCDC.dll", EntryPoint = "GetApplVers", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool GetApplVers(byte[] applVers);

            [DllImport("VisionARCDC.dll", EntryPoint = "GetStatus", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool GetStatus(byte[] stat);

            [DllImport("VisionARCDC.dll", EntryPoint = "GetErrors", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool GetErrors(ref Int32 num, UInt16[] codes);

            [DllImport("VisionARCDC.dll", EntryPoint = "LedCamera", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool LedCamera(byte color);

            [DllImport("VisionARCDC.dll", EntryPoint = "Reset", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool Reset();

            [DllImport("VisionARCDC.dll", EntryPoint = "ResetKeys", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool ResetKeys();

            [DllImport("VisionARCDC.dll", EntryPoint = "PowerOffLcd", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool PowerOffLcd();

            [DllImport("VisionARCDC.dll", EntryPoint = "PowerOnLcd", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool PowerOnLcd();

            [DllImport("VisionARCDC.dll", EntryPoint = "PowerOffCamera", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool PowerOffCamera();

            [DllImport("VisionARCDC.dll", EntryPoint = "PowerOnCamera", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool PowerOnCamera();
        }

        private static readonly string[] st_fb = { "Init", "Start", "Idle", "Header", "Checksum", "Payload", "Error" };
        private static readonly string[] st_cdc = { "Init", "Start", "Idle", "Header", "Checksum", "Payload", "Reply", "Send", "Error" };
        private const byte GREEN = 0, RED = 2, OFF = 3;
        private const int WIDTH = 419, HEIGTH = 138;
        private const int MAX_CHARS_TEXTBOX = 36;
        private const int MAX_LINES_TEXTBOX = 6;

        private readonly Callback callback;
        private readonly QRDecoder decoder = new QRDecoder();

        private readonly List<WebCameraId> cameras = new List<WebCameraId>();


        public ErGlassController()
        {
            InitializeComponent();

            if (!VisionARDLL.Startup() || !VisionARDLL.StartupCdc())
            {
                MessageBox.Show("Please make sure the glasses are connected", "Glasses not found",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                Environment.Exit(-1);
            }

            PopulateCameraCheckBox();

            callback = new Callback(OnKeyChanged);
        }


        private void ErGlassDemo_Load(object sender, EventArgs e)
        {
            PopulateInfoTextBox();
            CentralPictureBox.Image = new Bitmap(Properties.Resources.tag, WIDTH, HEIGTH);
        }


        private void ErGlassController_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopBtn_Click(sender, e);

            if (KryptonCheckBoxEnableTouchpad.Checked)
            {
                if (!VisionARDLL.StopTouchpad())
                {
                    Console.WriteLine("error stop touchpad");
                }
            }

            if (!VisionARDLL.CloseDevFb())  Console.WriteLine("error devicefb close");
            if (!VisionARDLL.CloseDevCdc()) Console.WriteLine("error devicecdc close");
        }


        private static byte[] BitmapToByteArray(Bitmap img)
        {
            Rectangle rect        = new Rectangle(0, 0, img.Width, img.Height);
            BitmapData bitmapData = img.LockBits(rect, ImageLockMode.ReadOnly, img.PixelFormat);
            int length            = bitmapData.Stride * bitmapData.Height;

            byte[] bytes = new byte[length];

            Marshal.Copy(bitmapData.Scan0, bytes, 0, length);
            img.UnlockBits(bitmapData);
            return bytes;
        }


        private void DisplayBtn_Click(object sender, EventArgs e)
        {
            using (Bitmap img = new Bitmap(CentralPictureBox.Image))
            {
                byte[] bytes = BitmapToByteArray(img);

                if (!VisionARDLL.WriteImg(bytes))
                {
                    Console.WriteLine("error WriteImg");
                }
            }
        }


        private void ScreenshotBtn_Click(object sender, EventArgs e)
        {
            Bitmap img = new Bitmap(Width, Height);
            DrawToBitmap(img, new Rectangle(0, 0, Width, Height));

            byte[] bytes = BitmapToByteArray(new Bitmap(img, WIDTH, HEIGTH));

            if (!VisionARDLL.WriteImg(bytes))
            {
                Console.WriteLine("error WriteImg");
            }
        }


        private void BrowseBtn_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    if (dlg.FileName.Length != 0)
                    {
                        using (Bitmap img = new Bitmap(dlg.FileName))
                        {
                            CentralPictureBox.Image = new Bitmap(img, WIDTH, HEIGTH);

                            byte[] bytes = BitmapToByteArray(new Bitmap(img, WIDTH, HEIGTH));

                            if (!VisionARDLL.WriteImg(bytes))
                            {
                                Console.WriteLine("error WriteImg");
                            }
                        }
                    }
                }
            }
        }


        private void KryptonButtonSend_Click(object sender, EventArgs e)
        {
            byte[] bytes = BitmapToByteArray(new Bitmap(TextToBitmap(KryptonTextBox.Text), WIDTH, HEIGTH));

            if (!VisionARDLL.WriteImg(bytes))
            {
                Console.WriteLine("error WriteImg");
            }
        }


        private Bitmap TextToBitmap(string text)
        {
            Bitmap objBmpImage = new Bitmap(1, 1);
            Font objFont = new Font(FontFamily.GenericMonospace, 18, FontStyle.Bold, GraphicsUnit.Pixel);

            objBmpImage = new Bitmap(objBmpImage, new Size(419, 138));

            Graphics objGraphics = Graphics.FromImage(objBmpImage);

            objGraphics.Clear(Color.Empty);
            objGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            objGraphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            objGraphics.DrawString(text, objFont, new SolidBrush(Color.FromArgb(255, 255, 255)), 6, 0);
            objGraphics.Flush();

            return objBmpImage;
        }


        private void PopulateInfoTextBox()
        {
            byte[] protVers = new byte[2];
            byte[] bootVers = new byte[2];
            byte[] applVers = new byte[6];
            byte[] status   = new byte[5];

            if (VisionARDLL.GetProtVers(protVers) &&
                VisionARDLL.GetBootVers(bootVers) &&
                VisionARDLL.GetApplVers(applVers) &&
                VisionARDLL.GetStatus(status))
            {
                KryptonLabelProtFill.Text  = $" {protVers[0]}.{protVers[1]}\r\n";
                KryptonLabelBootFill.Text  = $" {bootVers[0]}.{bootVers[1]}\r\n";
                KryptonLabelVerFill.Text   = $" {applVers[0]}.{applVers[1]}.{applVers[2]}\r\n";
                KryptonLabelDateFill.Text  = $" {applVers[3]} {MonthFromInt(applVers[4])} 20{applVers[5]}\r\n";
                KryptonLabelCdcFill.Text   = $" {st_cdc[status[1]]}\r\n";
                KryptonLabelErGlsFill.Text = $" {st_fb[status[0]]}\r\n";
            }
        }


        private void PopulateCameraCheckBox()
        {
            KryptonComboBoxCamera.Items.Clear();

            foreach (WebCameraId c in WebCameraControl.GetVideoCaptureDevices())
            {
                if (!(c is null))
                {
                    KryptonComboBoxCamera.Items.Add(c.Name);
                    cameras.Add(c);

                    KryptonComboBoxCamera.SelectedIndex = 0;
                }
            }

            if (cameras.Count == 0)
            {
                KryptonGroupBoxLed.Enabled          = false;
                KryptonButtonStartCapturing.Enabled = false;
                KryptonButtonStopCapturing.Enabled  = false;
                KryptonButtonCapture.Enabled        = false;
            }
        }


        private static string MonthFromInt(byte month)
        {
            switch (month)
            {
                case 1:  return "Jan";
                case 2:  return "Feb";
                case 3:  return "Mar";
                case 4:  return "Apr";
                case 5:  return "May";
                case 6:  return "Jun";
                case 7:  return "Jul";
                case 8:  return "Aug";
                case 9:  return "Sep";
                case 10: return "Oct";
                case 11: return "Nov";
                case 12: return "Dec";
                default: return "moriremo tutti";
            }
        }


        private void BrightnessTrackbar_Scroll(object sender, EventArgs e)
        {
            if (!VisionARDLL.Brightness(KryptonTrackBarBrightness.Value)) Console.WriteLine("error Brightness");
        }


        private void ContrastTrackbar_Scroll(object sender, EventArgs e)
        {
            if (!VisionARDLL.Contrast(KryptonTrackBarContrast.Value)) Console.WriteLine("error Contrast");
        }


        private void StartBtn_Click(object sender, EventArgs e)
        {
            if (cameras.Count != 0)
            {
                WebCameraControl.StartCapture(cameras[KryptonComboBoxCamera.SelectedIndex]);

                if (!VisionARDLL.LedCamera(RED)) Console.WriteLine("error Led");

                UpdateEnabledButtons();
                KryptonRadioButtonRed.Checked = true;
            }
        }


        private void StopBtn_Click(object sender, EventArgs e)
        {
            if (WebCameraControl.IsCapturing) WebCameraControl.StopCapture();

            if (!VisionARDLL.LedCamera(OFF)) Console.WriteLine("error Led");

            UpdateEnabledButtons();
            KryptonRadioButtonOff.Checked = true;
        }


        private void CaptureBtn_Click(object sender, EventArgs e)
        {
            using (Bitmap img = new Bitmap(WebCameraControl.GetCurrentImage(), WIDTH, HEIGTH))
            {
                byte[] bytes = BitmapToByteArray(img);

                if (!VisionARDLL.WriteImg(bytes))
                {
                    Console.WriteLine("error WriteImg");
                }
            }
        }


        private void ReadQRBtn_Click(object sender, EventArgs e)
        {
            using (Bitmap img = new Bitmap(WebCameraControl.GetCurrentImage()))
            {
                byte[][] dataByteArray = decoder.ImageDecoder(img);

                if (dataByteArray is null)
                {
                    Console.WriteLine("no QR Code detected");
                    return;
                }

                string str = ByteArrayToStr(dataByteArray[0]);  // one QR Code at the time (no nested QRs)

                byte[] bytes = BitmapToByteArray(new Bitmap(TextToBitmap(str), WIDTH, HEIGTH));

                if (!VisionARDLL.WriteImg(bytes))
                {
                    Console.WriteLine("error WriteImg");
                }
            }
        }


        private void GreenRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (KryptonRadioButtonGreen.Checked)
            {
                KryptonRadioButtonRed.Checked = false;
                KryptonRadioButtonOff.Checked = false;

                if (!VisionARDLL.LedCamera(GREEN)) Console.WriteLine("error led green");
            }
        }


        private void RedRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (KryptonRadioButtonRed.Checked)
            {
                KryptonRadioButtonGreen.Checked = false;
                KryptonRadioButtonOff.Checked   = false;

                if (!VisionARDLL.LedCamera(RED)) Console.WriteLine("error led red");
            }
        }


        private void OffRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (KryptonRadioButtonOff.Checked)
            {
                KryptonRadioButtonRed.Checked   = false;
                KryptonRadioButtonGreen.Checked = false;

                if (!VisionARDLL.LedCamera(OFF)) Console.WriteLine("error led off");
            }
        }


        private void UpdateEnabledButtons()
        {
            KryptonButtonStartCapturing.Enabled = !WebCameraControl.IsCapturing;
            KryptonButtonStopCapturing.Enabled  = WebCameraControl.IsCapturing;
            KryptonButtonCapture.Enabled        = WebCameraControl.IsCapturing;
            KryptonButtonReadQR.Enabled         = WebCameraControl.IsCapturing;

            KryptonRadioButtonGreen.Enabled = !WebCameraControl.IsCapturing;
            KryptonRadioButtonRed.Enabled   = !WebCameraControl.IsCapturing;
            KryptonRadioButtonOff.Enabled   = !WebCameraControl.IsCapturing;

            KryptonComboBoxCamera.Enabled = !WebCameraControl.IsCapturing;
        }


        private void VibrationBtn_Click(object sender, EventArgs e)
        {
            if (!VisionARDLL.Haptic(Convert.ToInt32(KryptonNumericUpDownVibration.Value))) Console.WriteLine("error Haptic");
        }


        private static string ByteArrayToStr(byte[] dataArray)
        {
            var Decoder = Encoding.UTF8.GetDecoder();
            int CharCount = Decoder.GetCharCount(dataArray, 0, dataArray.Length);
            char[] CharArray = new char[CharCount];
            Decoder.GetChars(dataArray, 0, dataArray.Length, CharArray, 0);
            return new string(CharArray);
        }


        private void GetImu()
        {
            float[] accValues  = new float[3];
            float[] gyroValues = new float[3];

            if (VisionARDLL.GetImuAcc(accValues) && VisionARDLL.GetImuGyro(gyroValues))
            {
                KryptonLabelAccX.Text = $"X = {Math.Round(accValues[0], 2)} [m/s\u00b2]";
                KryptonLabelAccY.Text = $"Y = {Math.Round(accValues[1], 2)} [m/s\u00b2]";
                KryptonLabelAccZ.Text = $"Z = {Math.Round(accValues[2], 2)} [m/s\u00b2]";

                KryptonLabelGyroX.Text = $"X = {Math.Round(gyroValues[0], 5)} [rad/s]";
                KryptonLabelGyroY.Text = $"Y = {Math.Round(gyroValues[1], 5)} [rad/s]";
                KryptonLabelGyroZ.Text = $"Z = {Math.Round(gyroValues[2], 5)} [rad/s]";
            }
            else
            {
                Console.WriteLine("error getting imu");
            }
        }


        private void TimerImu_Tick(object sender, EventArgs e)
        {
            TimerImu.Stop();
            GetImu();
            TimerImu.Start();
        }


        private void ReadSensorsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            TimerImu.Enabled = KryptonCheckBoxSensors.Checked;
        }


        private void KryptonTextBox_TextChanged(object sender, EventArgs e)
        {
            if (KryptonTextBox.Text == "") return;

            string[] sTextArray = KryptonTextBox.Text.Split(new string[]
            {
                Environment.NewLine
            }, StringSplitOptions.RemoveEmptyEntries);

            int nLines = sTextArray.Length;
            string sLastLine = sTextArray[nLines - 1];

            if (sLastLine.Length > MAX_CHARS_TEXTBOX)
            {
                int nTextLen = KryptonTextBox.Text.Length;
                string sText = KryptonTextBox.Text.Substring(0, nTextLen - 1) + Environment.NewLine + KryptonTextBox.Text[nTextLen - 1];

                KryptonTextBox.Text = sText;
                KryptonTextBox.SelectedText = "";
                KryptonTextBox.Select(nTextLen + 2, 0);
            }

            if (KryptonTextBox.Lines.Length > MAX_LINES_TEXTBOX)
            {
                KryptonTextBox.Text = KryptonTextBox.Text.Substring(0, KryptonTextBox.Text.LastIndexOf(Environment.NewLine));

                KryptonTextBox.Select(0, 0);
                KryptonTextBox.ScrollToCaret();

                MessageBox.Show($"Only {MAX_LINES_TEXTBOX} lines are allowed.");
            }
        }


        private void KryptonButtonClear_Click(object sender, EventArgs e)
        {
            KryptonTextBox.Text = "";
        }


        private void VibrUpDown_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 /* Keys.Enter */)
            {
                VibrationBtn_Click(sender, e);
            }
        }


        private void OnKeyChanged(byte key)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegate
                {
                    Console.WriteLine("Pressed Key: " + key);
                    switch ((Keys)key)
                    {
                        case Keys.F1:
                        case Keys.F2:
                        case Keys.F3:
                        case Keys.F4:
                            if (Disposing == false)
                            {
                                KryptonLabelTouchpad.Text = "Single tap";
                            }
                            break;

                        case Keys.F5:
                        case Keys.F6:
                        case Keys.F7:
                        case Keys.F8:
                            if (Disposing == false)
                            {
                                KryptonLabelTouchpad.Text = "Double tap";
                            }
                            break;

                        case Keys.F9:
                            if (Disposing == false)
                            {
                                KryptonLabelTouchpad.Text = "Swipe down";
                            }
                            break;

                        case Keys.F10:
                            if (Disposing == false)
                            {
                                KryptonLabelTouchpad.Text = "Swipe up";
                            }
                            break;
                    }
                }));
            }
        }


        private void EnableTouchpadCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (KryptonCheckBoxEnableTouchpad.Checked)
            {
                if (!VisionARDLL.StartTouchpad(callback, 250))
                {
                    Console.WriteLine("error start touchpad");
                }
            }
            else
            {
                if (!VisionARDLL.StopTouchpad())
                {
                    Console.WriteLine("error stop touchapad");
                }
            }
        }
    }


    internal delegate void Callback(byte key);
}
