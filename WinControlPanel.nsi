; WinControlPanel.nsi
;
; It will install "WinControlPanel" application for Windows into a directory that the user selects,
; creating start menu and desktop links, according to user choice.

;--------------------------------


; The name of the installer
Name "VisionAR Control Panel"

; The file to write
OutFile "visionarcontrolpanel.exe"

; Installer Icon
Icon "VisionAR.ico"

; Request application privileges for Windows Vista and higher
RequestExecutionLevel admin

; Build Unicode installer
Unicode True

; The default installation directory
InstallDir "C:\ETL\VisionARControlPanel"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\VisionARControlPanel" "Install_Dir"

;--------------------------------

!include CheckAndDownloadVCRedist.nsh
!include CheckAndDownloaddotnet.nsh

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "VisionAR Control Panel (required)"

  SectionIn RO
  
  Call CheckAndDownloadVCRedist2015
  Call CheckAndDownloadDotNet472

  CreateDirectory "$INSTDIR"
  
  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"
  
  ; Put file there
  File "WinControlPanel\bin\Release\WinControlPanel.exe.config"
  File "WinControlPanel\bin\Release\VisionARCDC.dll"
  File "WinControlPanel\bin\Release\VisionARFB.dll"
  File "WinControlPanel\bin\Release\WinControlPanel.exe"
  File "WinControlPanel\bin\Release\Krypton Docking.dll"
  File "WinControlPanel\bin\Release\Krypton Navigator.dll"
  File "WinControlPanel\bin\Release\Krypton Ribbon.dll"
  File "WinControlPanel\bin\Release\Krypton Toolkit.dll"
  File "WinControlPanel\bin\Release\Krypton Workspace.dll"
  File "WinControlPanel\bin\Release\QRCodeDecoderLibrary.dll"
  File "WinControlPanel\bin\Release\System.Drawing.Common.dll"
  File "WinControlPanel\bin\Release\WebEye.Controls.WinForms.WebCameraControl.dll"
  File "VisionAR.ico"

  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\VisionARControlPanel "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "DisplayName" "VisionAR Control Panel"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "InstallLocation" '"$INSTDIR"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "Publisher" "Univet s.r.l."
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "URLInfoAbout" "www.univetar.com"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "DisplayVersion" "3.2"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "VersionMajor" 3
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "VersionMinor" 2
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "NoRepair" 1
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel" "DisplayIcon" "$INSTDIR\VisionAR.ico,0"


  WriteUninstaller "$INSTDIR\uninstall.exe"
  
SectionEnd


; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  ; Set shortcut working path to the installation directory.
  SetOutPath "$INSTDIR"

  CreateDirectory "$SMPROGRAMS\VisionARControlPanel"
  CreateShortcut "$SMPROGRAMS\VisionARControlPanel\Uninstall.lnk" "$INSTDIR\uninstall.exe"
  CreateShortcut "$SMPROGRAMS\VisionARControlPanel\VisionAR Control Panel.lnk" "$INSTDIR\WinControlPanel.exe" "" "$INSTDIR\VisionAR.ico"

SectionEnd

; Optional section (can be disabled by the user)
Section "Desktop Shortcut"

  ; Set shortcut working path to the installation directory.
  SetOutPath "$INSTDIR"
  
  CreateShortcut "$DESKTOP\VisionAR Control Panel.lnk" "$INSTDIR\WinControlPanel.exe" "" "$INSTDIR\VisionAR.ico"

SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARControlPanel"
  DeleteRegKey HKLM SOFTWARE\VisionARControlPanel

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\VisionARControlPanel\*.lnk"
  Delete "$DESKTOP\VisionAR Control Panel.lnk"

  ; Remove files and uninstaller
  Delete "$INSTDIR\WinControlPanel.exe.config"
  Delete "$INSTDIR\VisionARCDC.dll"
  Delete "$INSTDIR\VisionARFB.dll"
  Delete "$INSTDIR\WinControlPanel.exe"
  Delete "$INSTDIR\Krypton Docking.dll"
  Delete "$INSTDIR\Krypton Navigator.dll"
  Delete "$INSTDIR\Krypton Ribbon.dll"
  Delete "$INSTDIR\Krypton Toolkit.dll"
  Delete "$INSTDIR\Krypton Workspace.dll"
  Delete "$INSTDIR\QRCodeDecoderLibrary.dll"
  Delete "$INSTDIR\System.Drawing.Common.dll"
  Delete "$INSTDIR\WebEye.Controls.WinForms.WebCameraControl.dll"
  Delete "$INSTDIR\VisionAR.ico"
  Delete "$INSTDIR\uninstall.exe"

  ; Remove directories
  RMDir "$SMPROGRAMS\VisionARControlPanel"
  RMDir /r "$INSTDIR"

SectionEnd
